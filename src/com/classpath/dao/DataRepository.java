package com.classpath.dao;

import java.util.*;

import com.classpath.model.*;

public class DataRepository {
	Set<Owner> owners;
	
	public DataRepository() {
		owners=new HashSet<>();
	}
	
	public Owner addOwner(Owner owner) {
		boolean x=owners.add(owner);
		if(x) {
			return owner;
		}
		else
			return null;
	}
	public Owner findOwnerById(int id) {
		Iterator<Owner> it=owners.iterator();
		while(it.hasNext()) {
			Owner e=it.next();
			if(e.getOwnerId()==id)
				return e;
		}
		return null;
	}
	public Owner deleteOwnerById(int id) {
		Iterator<Owner> it=owners.iterator();
		while(it.hasNext()) {
			Owner e=it.next();
			if(e.getOwnerId()==id) {
				owners.remove(e);
				return e;
			}
		}
		return null;
	}
	public Owner updateOwnerById(int id, Owner em) {
		Iterator<Owner> it=owners.iterator();
		while(it.hasNext()) {
			Owner e=it.next();
			if(e.getOwnerId()==id) {
				owners.remove(e);
				owners.add(em);
				return e;
			}
		}
		return null;
	}
}
