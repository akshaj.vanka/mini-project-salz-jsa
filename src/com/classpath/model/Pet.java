package com.classpath.model;

import java.util.Objects;

//Pet 
//id - long 
//name - String 
//type - CAT, DOG, RABBIT


public class Pet {
	private long petId;
	private static long id=1;
	private String name, type;
	public Pet(String name, String type) {
		super();
		this.petId = id++;
		this.name = name;
		this.type = type;
	}
	
	public long getPetId() {
		return petId;
	}

	public void setPetId(long petId) {
		this.petId = petId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Pet [petId=" + petId + ", name=" + name + ", type=" + type + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(petId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pet other = (Pet) obj;
		return petId == other.petId;
	}
	
}
