package com.classpath.model;

import java.time.*;
import java.util.Objects;

//Visit 
//  Pet
//  Owner
//  Vet 
//  LocalDateTime

public class Visit {
	private Pet pet;
	private Owner owner;
	private Vet vet;
	private LocalDateTime appointmentDateTime;
	public Visit(Pet pet, Owner owner, Vet vet, LocalDateTime appointmentDateTime) {
		super();
		this.pet = pet;
		this.owner = owner;
		this.vet = vet;
		this.appointmentDateTime = appointmentDateTime;
	}
	public Pet getPet() {
		return pet;
	}
	public void setPet(Pet pet) {
		this.pet = pet;
	}
	public Owner getOwner() {
		return owner;
	}
	public void setOwner(Owner owner) {
		this.owner = owner;
	}
	public Vet getVet() {
		return vet;
	}
	public void setVet(Vet vet) {
		this.vet = vet;
	}
	public LocalDateTime getAppointmentDateTime() {
		return appointmentDateTime;
	}
	public void setAppointmentDateTime(LocalDateTime appointmentDateTime) {
		this.appointmentDateTime = appointmentDateTime;
	}
	@Override
	public String toString() {
		return "Visit [pet=" + pet + ", owner=" + owner + ", vet=" + vet + ", appointmentDateTime="
				+ appointmentDateTime + "]";
	}
	@Override
	public int hashCode() {
		return Objects.hash(appointmentDateTime, owner, pet, vet);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Visit other = (Visit) obj;
		return Objects.equals(appointmentDateTime, other.appointmentDateTime) && Objects.equals(owner, other.owner)
				&& Objects.equals(pet, other.pet) && Objects.equals(vet, other.vet);
	}
	
}
