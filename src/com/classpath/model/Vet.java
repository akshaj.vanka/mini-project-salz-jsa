package com.classpath.model;

import java.util.Objects;

//Vet 
//id - long 
//name - String 

public class Vet {
	private long vetId;
	private static long id=1;
	private String name;
	public Vet(String name) {
		super();
		this.vetId = id++;
		this.name = name;
	}
	public long getVetId() {
		return vetId;
	}
	public void setVetId(long vetId) {
		this.vetId = vetId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Vet [vetId=" + vetId + ", name=" + name + "]";
	}
	@Override
	public int hashCode() {
		return Objects.hash(vetId);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vet other = (Vet) obj;
		return vetId == other.vetId;
	}
	
}
