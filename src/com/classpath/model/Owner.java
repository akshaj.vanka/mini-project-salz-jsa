package com.classpath.model;

import java.util.*;

//Owner
//id - long
//name - String
//Address - String
//City - String
//phone - String
//Set<Pet> 

public class Owner {
	private long ownerId;
	private static long id=1;
	private String name, address, city, phoneNumber;
	private Set<Pet> pets;
	public Owner(String name, String address, String city, String phoneNumber) {
		super();
		this.ownerId = id++;
		this.name = name;
		this.address = address;
		this.city = city;
		this.phoneNumber = phoneNumber;
		this.pets = new HashSet<>();
	}
	public long getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(long ownerId) {
		this.ownerId = ownerId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Set<Pet> getPets() {
		return pets;
	}
	public void setPets(Set<Pet> pets) {
		this.pets = pets;
	}
	@Override
	public String toString() {
		String st= "Owner [ownerId=" + ownerId + ", name=" + name + ", address=" + address + ", city=" + city
				+ ", phoneNumber=" + phoneNumber + "]";
		Iterator<Pet> it=pets.iterator();
		while(it.hasNext())
			st+=it.next();
		return st;
	}
	@Override
	public int hashCode() {
		return Objects.hash(ownerId);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Owner other = (Owner) obj;
		return ownerId == other.ownerId;
	}
	
	
}
